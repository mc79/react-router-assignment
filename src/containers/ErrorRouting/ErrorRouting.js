import React from 'react';

export default function ErrorRouting() {
    return (
        <div style={{ textAlign: 'center'}}>
            <h4>Path not found (error 404)</h4>
        </div>
    )
}